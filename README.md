# TOOLBOX

Toolbox est une bibliothèque de service utilisables au travers d'Android Debug Bridge permettant d'effectuer des opérations autrement inaccessibles.

## AddToClipboard

Envoi le texte passé en paramètres dans le presse papier tu terminal.

```bash
adb shell am startservice -a AddToClipboard -e text "TEXT_TO_SEND"
```

## OutputAllContacts

Ecrit l'ensemble des contacts enregistrés dans le terminal, dans les logs de ce dernier. Les logs sortis de cette manière sont préfixés "CONTACTS"

```bash
adb shell am startservice ./OutputAllContacts
```
Cette commande nécessite l'accés au Contacts du terminal. Il est possible d'autoriser l'application via ADB avec la commande suivante : 

```bash
adb shell pm grant com.stardust.toolbox android.permission.READ_CONTACTS
```

## SMS Broadcast



Cette fonction nécéssite l'accés au SMS du terminal. Il est possible d'autoriser l'application via ADB avec la commande suivante : 

```bash
adb shell pm grant com.stardust.toolbox android.permission.READ_SMS
```