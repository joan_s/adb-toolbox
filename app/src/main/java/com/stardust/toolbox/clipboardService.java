package com.stardust.toolbox;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.widget.Toast;

public class clipboardService extends JobIntentService {
    public clipboardService() {}

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
    }

    public int onStartCommand(Intent intent, int flags, int startId)
    {
        // Get passed arguments
        String text = intent.getStringExtra("text");
        ClipData clip = ClipData.newPlainText("simple text", text);

        // Get Clipboard running instance
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

        // Fill Clipboard
        clipboard.setPrimaryClip(clip);

        // Display confirmation toast
        Context context = getBaseContext();
        int duration = Toast.LENGTH_SHORT;
        String content = "Added to clipboard";
        Toast toast = Toast.makeText(context, content, duration);
        toast.show();

        // Stop this
        stopForeground(true);
        this.stopSelf();
        return START_NOT_STICKY;
    }
}
