package com.stardust.toolbox;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;
import android.widget.Toast;

public class contactsService extends JobIntentService{

    private static final String TAG = "CONTACTS";

    public contactsService() {}

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if (( cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                // Contact ID
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                // Contact Name
                String name = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{ id }, null);

                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));

                        String email = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Email.ADDRESS));

                        Log.i(TAG, "Name: " + name);
                        Log.i(TAG, "Phone Number: " + phoneNo);
                        Log.i(TAG, "Email: " + email);
                    }
                    pCur.close();
                }
            }
         }
         if (cur != null) {
            cur.close();
         }

        // Display confirmation toast
        Context context = getBaseContext();
        int duration = Toast.LENGTH_SHORT;
        String content = "Contacts sended through logcat";
        Toast toast = Toast.makeText(context, content, duration);
        toast.show();

        return START_NOT_STICKY;
    }
}
